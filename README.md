> This project is discontinued. I suggest using [Plume](https://joinplu.me) instead; An [ActivityPub](https://activitypub.rocks/) federated blogging application written in [Rust](https://www.rust-lang.org/)!

# Open Persian Django Social
![License: GPL-3](https://img.shields.io/badge/License-GPL--3-brightgreen)

An open source social written with [Python](https://www.python.org/) and [Django webframework](https://www.djangoproject.com/). You can run your own instace and write freely!

## Features
- 🚀 Open source
- 📜 Post based
- 😎 Complete profile

## Run your own instace
After downloading and extracting this repository, open terminal and run these commands:

Installing virtual envirement:
```
pip3 install virtualenv
```

Creating a virtual envirement:
```
vitualenv -p python3 venv
```

Activate your virtual envirement:
```
source venv/bin/activate
```
In Windows: `venv\Scripts\activate`

Install requirements:
```
pip install -r requirements.txt
```

Migrating:
```
python manage.py migrate
```

Create a super user:
```
python manage.py createsuperuser
```

Run:
```
python manage.py runserver
```

Open `localhost:8000` in your browser to see it!

## License
This project uses [GPL-3](https://codeberg.org/mskf1383/Open-Persian-Django-Social/src/branch/main/LICENSE) license.
